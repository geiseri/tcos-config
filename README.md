# Thin Client Operating System Configuration Utility #

## Configure ##

The configuration system relies on a certian amount of infrastructure to operate.  The thin client
is configured by a plain text `.ini` file that is hosted on a remote http/https server.  This file
is automatically searched via DNS and DHCP settings.

### DHCP ###

For the system to be able to autoconfigure the DHCP server needs to have a specific configuration.

![DHCP Configuration](dhcp-config.png)

1. Option 003 - The router
1. Option 006 - The DNS servers
1. Option 015 - The DNS domain name
1. Option 042 - The NTP servers

### DNS ###

The system is bootstrapped using DNS SRV records.  Each record contains the URL to the webserver that contains the `.ini` files.

![DNS Configuration](dns-config.png)

1. Create a SRV record `_tcos._tcp` in your selected domain. This should be the domain set by DHCP option `015`
1. Set the `server` and `port` to the settings for the web server that hosts the `.ini` files.
1. Multiple entries can be created for failover and load balance.

### Webserver ###

1. Create a configuration `ini` file on the webserver called `tcos.ini` for common application defaults.
1. For custom configuration properties for individual thin clients create a a file `<mac address>.ini`.  Where `<mac address>` is the mac address of the thin client.  This is formatted as upper case with each octet delimited by a `-` charicter.
1. For custom configuration properties for types of thin clients the "machine" and "distro" names can also be used.  Example: `baytrail-64.ini` or `tcos-mini.ini`.
1. These files must be in the root directory so it is reccomnended to store them on their own vhost.

### INI Files ###

Structure:

``` ini
[group]
key = value
```

System Configuration Properties:

| Group | Key | Description |
|:--- |:--- |:--- |
| hardware | keymap | Keymap for the system keyboard attached to the thin client |
| hardware | dpms | Screen poweroff timeout for the thin client |
| hardware | sound | Enable audio playback and recording on the thin client |
| hardware | autohide_mouse | Hide the mouse when not in use |
| locale | timezone | Timezone for the thinclient |
| locale | language | Language for local applications on the thin client |
| firmware | md5sum | The md5 hash for the new firmware package  |
| firmware | url | The download path for the new firmware package  |

Browser Connection Configuration Properties:

| Group | Key | Description |
|:--- |:--- |:--- |
| connection | type | This should always be `browser` for this connection type |
| connection | start_url | The URL that is opened at launch |
| connection | whitelist_permissions | Allow all permissions by default |
| connection/permissions | allow_back_button | Enable the "Back" button in the UI |
| connection/permissions | allow_reload_button | Enable the "Reload" button in the UI |
| connection/permissions | allow_url_edit | Allow the user to change the URL |
| connection/permissions | allow_browser_close | Allow the user to exit the browser |
| connection/permissions | allow_create_tabs | Allow the user to create new tabs in the browser UI |
| connection/permissions | allow_uploads | Allow the user to be able to upload files to websites |
| connection/permissions | allow_downloads | Allow the user to download files from websites |
| connection/permissions | allow_html5_notifications | Allow HTML5 notifications to appear in the UI |
| connection/permissions | allow_html5_geolocation | Enable location services in the browser |
| connection/permissions | allow_html5_media_audio_capture | Enable microphone access for websites |
| connection/permissions | allow_html5_media_video_capture | Enable webcam access for websites |
| connection/permissions | allow_html5_media_audio_video_capture | Enable both webcam and microphone access for websites |
| connection/permissions | allow_html5_mouse_lock | Allow websites to lock the mouse |
| connection/permissions | allow_ssl_invalid_certificate_authority | Do not prompt warnings for invalid CAs |
| connection/external | rdp | Enable launching of RDP files |
| connection/external | pdf | Enable reading of PDF files |

RDP Connection Configuration Properties:

| Group | Key | Description |
|:--- |:--- |:--- |
| connection | type | This should always be `rdp` for this connection type |
| connection | server | The remote server to connect to |
| connection | username | The default username to connect with |
| connection | domain | The default domain to connect with |
| connection | password | The default password to connect with |
| connection | branding_url | The URL to an image for the login dialog |
| connection | terms_of_service | Free text that is displayed in the login dialog |
| connection | bpp | Color depth of the connection |
| connection | auto_reconnect | Automatically reconnect on connection interupt |
| connection | reconnect_max_retries | Maximum times to attempt reconnection |
| connection | remote_fx | Enable RemoteFX features on the connection |
| connection | cleartype_fonts | Enable ClearType font smoothing on the remote desktop |
| connection | aero_effects | Enable Aero graphical effects on the remote desktop |
| connection | window_drag | Enable full window drags on the remote desktop |
| connection | menu_animations | Enable UI animations on the remote desktop |
| connection | themes | Enable custom themes on the remote desktop |
| connection | wallpaper | Draw the wallpaper on the remote desktop |
| connection | heartbeat | Enable the "heartbeat" feature of RDP |
| connection | multitransport | Enable UDP streams for RDP the connection |
| connection | display_control | Enable display control for the remote desktop |
| connection | echo | Enable connection telemetry for the RDP connection |
| connection | multimon | Use all local monitors on the remote desktop |
| connection | sound_output | Play audio from the remote desktop |
| connection | sound_input | Redirect local microphone to the remote desktop |
| connection | ignore_ssl_errors | Ignore SSL Certificate errors |
| connection | redirect_drives | Redirect local USB drives to the remote desktop |
| connection | clipboard | Redirect clipboard to the remote desktop |