#include <QString>
#include <QtTest>

#include <inihreaderwrapper.h>
#include <tcosconnectionbase.h>

class ConfigureTests : public QObject
{
    Q_OBJECT

public:
    ConfigureTests();

private Q_SLOTS:
    void testIniParser();
    void testConnectionFactory();

};

ConfigureTests::ConfigureTests()
{
}

void ConfigureTests::testIniParser()
{
    InihReaderWrapper ini;
    QByteArray data = "[section]\n" \
                      "text_key = text value\n" \
                      "bool_key = true\n" \
                      "number_key = 10\n" \
                      "[nested/section]\n" \
                      "text_key = nested text value\n" \
                      "bool_key = false\n" \
                      "number_key = 20\n";

    QVERIFY2(ini.parse(data), "Parse Failure");

    QCOMPARE(ini.value<QString>("section", "text_key", QString()), QString("text value"));
    QCOMPARE(ini.value<bool>("section", "bool_key", false), true);
    QCOMPARE(ini.value<int>("section", "number_key", 0), 10);

    QCOMPARE(ini.value<QString>("nested/section", "text_key", QString()), QString("nested text value"));
    QCOMPARE(ini.value<bool>("nested/section", "bool_key", true), false);
    QCOMPARE(ini.value<int>("nested/section", "number_key", 0), 20);

    QCOMPARE(ini.value<QVariantMap>("nested", "section", QVariantMap())["text_key"].toString(), QString("nested text value"));
    QCOMPARE(ini.value<QVariantMap>("nested", "section", QVariantMap())["bool_key"].toBool(), false);
    QCOMPARE(ini.value<QVariantMap>("nested", "section", QVariantMap())["number_key"].toInt(), 20);
}

void ConfigureTests::testConnectionFactory()
{
    InihReaderWrapper ini;
    QByteArray data = "[global]\n" \
                      "dummy_key = dummy value\n" \
                      "[connection]\n" \
                      "type = browser\n" \
                      "start_url = http://someserver.local/app\n" \
                      "whitelist_permissions = false\n" \
                      "[connection/permissions]\n" \
                      "allow_back_button = true\n" \
                      "allow_reload_button = true\n" \
                      "allow_url_edit = true\n" \
                      "\n";

    QBuffer outputBuffer;
    outputBuffer.open(QIODevice::WriteOnly);

    QVERIFY2(ini.parse(data), "Parse Failure");

    QSharedPointer<TcosConnectionBase> browser = TcosConnectionBase::factory(ini.section(QLatin1String("connection")));
    QVERIFY2(browser->isValid(), "Invalid connection");

    browser->writeConfiguration(&outputBuffer);

    QVERIFY2( outputBuffer.buffer().contains("StartUrl"), "Cannot read nested data");

}

QTEST_APPLESS_MAIN(ConfigureTests)

#include "tst_configuretests.moc"
