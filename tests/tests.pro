QT       += testlib dbus network
QT       -= gui

TARGET = tst_configuretests
CONFIG   += console
CONFIG   -= app_bundle
CONFIG   += c++11

SOURCES += \
        tst_configuretests.cpp

DEFINES += SRCDIR=\\\"$$PWD/\\\"

LIBS += -L$$OUT_PWD/../core/ -lcore
INCLUDEPATH += $$PWD/../core $$PWD/../core/qtpromise/include
DEPENDPATH += $$PWD/../core
PRE_TARGETDEPS += $$OUT_PWD/../core/libcore.a
