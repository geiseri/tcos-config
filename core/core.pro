QT       += network dbus
QT       -= gui

TARGET = core
TEMPLATE = lib
CONFIG += staticlib

DEFINES += QT_DEPRECATED_WARNINGS \
    INI_ALLOW_MULTILINE=0 \
    INI_HANDLER_LINENO=1

SOURCES += linkresolver.cpp \
    tcosconfiguration.cpp \
    linkfetcher.cpp \
    systemcontroller.cpp \
    configurationmanager.cpp \
    utilities.cpp \
    psplashcontroller.cpp \
    firmwareconfiguration.cpp \
    errortypes.cpp \
    inih/ini.c \
    inihreaderwrapper.cpp \
    tcosrdpconnection.cpp \
    tcosbrowserconnection.cpp \
    tcosconnectionbase.cpp \
    tcosinvalidconnection.cpp

CONFIG += link_pkgconfig
PKGCONFIG += qtsystemd

HEADERS += \
    linkresolver.h \
    tcosconfiguration.h \
    linkfetcher.h \
    systemcontroller.h \
    configurationmanager.h \
    utilities.h \
    psplashcontroller.h \
    firmwareconfiguration.h \
    errortypes.h \
    inih/ini.h \
    inihreaderwrapper.h \
    tcosrdpconnection.h \
    tcosbrowserconnection.h \
    tcosconnectionbase.h \
    tcosinvalidconnection.h

include(qtpromise/qtpromise.pri)
