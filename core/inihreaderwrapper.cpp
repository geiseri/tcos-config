#include "inihreaderwrapper.h"
#include "inih/ini.h"
#include "utilities.h"
#include <QDebug>

InihReaderWrapper::InihReaderWrapper()
{
}

bool InihReaderWrapper::parse(const QByteArray &buffer)
{
    m_settings.clear();
    return (0 == ::ini_parse_string(buffer.constData(), &InihReaderWrapper::line_handler, this));
}

QVariantMap InihReaderWrapper::section(const QString &name) const
{
    QStringList path = name.split(QLatin1Char('/'), QString::SkipEmptyParts);
    return Utilities::getAtPath(path, m_settings);
}

QVariant InihReaderWrapper::valueInternal(
    const QString &section, const QString &key, const QVariant &defaultValue) const
{
    QStringList path = section.split(QLatin1Char('/'), QString::SkipEmptyParts);
    QVariant result = Utilities::getAtPath(path, m_settings)[key];
    return result.isValid() ? result : defaultValue;
}

QVariant convertToVariant(const QString &value)
{
    if (value.isEmpty()) {
        return QVariant();
    }

    // bools
    if (value.compare(value, QLatin1String("true"), Qt::CaseInsensitive) == 0) {
        return QVariant::fromValue(true);
    }

    if (value.compare(value, QLatin1String("false"), Qt::CaseInsensitive) == 0) {
        return QVariant::fromValue(false);
    }

    // Numbers
    bool converted = false;

    if (value.contains(QLatin1Char('.'))) {
        double number = value.toDouble(&converted);

        if (converted) {
            return QVariant::fromValue(number);
        }
    } else {
        int number = value.toInt(&converted);
        if (converted) {
            return QVariant::fromValue(number);
        }
    }

    return value;
}

int InihReaderWrapper::line_handler(void *user, const char *section, const char *name, const char *value, int lineno)
{
    Q_UNUSED(lineno);

    InihReaderWrapper *reader = (InihReaderWrapper *)user;

    QString qSection = QString::fromUtf8(section);
    QString qValue = QString::fromUtf8(value);
    QStringList path = qSection.split(QLatin1Char('/'), QString::SkipEmptyParts);
    path << QString::fromUtf8(name);

    Utilities::setAtPath(path, &reader->m_settings, convertToVariant(qValue));
    return 1;
}
