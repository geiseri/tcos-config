#include "tcosrdpconnection.h"

#include <QJsonDocument>
#include <QJsonObject>
#include <QTextStream>

TcosRdpConnection::TcosRdpConnection(const QVariantMap &data) : TcosConnectionBase(data)
{
}

TcosConnectionBase::ConnectionType TcosRdpConnection::type() const
{
    return RDP;
}

void TcosRdpConnection::writeConfiguration(QIODevice *device) const
{
    QJsonDocument document;
    document.setObject(QJsonObject::fromVariantMap(configuration()));
    device->write(document.toJson());
}

void TcosRdpConnection::writeLauncher(QIODevice *device, const QString &config) const
{
    QTextStream output(device);
    output << QLatin1String("#!/bin/sh\n");
    output << QLatin1String("/usr/bin/x-window-manager&\n");
    output << QLatin1String("/usr/bin/rdpwrapper ") << config << QLatin1String("\n");
}
