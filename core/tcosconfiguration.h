#ifndef TCOSCONFIGURATION_H
#define TCOSCONFIGURATION_H

#include "firmwareconfiguration.h"
#include "inihreaderwrapper.h"
#include "tcosconnectionbase.h"
#include <QUrl>
#include <QVariant>

class TcosConfiguration
{
public:
    TcosConfiguration();

    bool parseConfiguration(const QByteArray &configuration);

    QString timezone(const QString &defaultValue) const;
    QString language(const QString &defaultValue) const;

    QString keymap(const QString &defaultValue) const;
    int dpms(quint8 defaultValue) const;
    bool mute(bool defaultValue) const;
    bool autohideMouse(bool defaultValue) const;

    QSharedPointer<TcosConnectionBase> connection() const;
    FirmwareConfiguration firmware() const;

    QUrl baseUrl() const;
    void setBaseUrl(const QUrl &baseUrl);

    bool isValid() const;

private:
    bool m_isValid;
    QUrl m_baseUrl;
    InihReaderWrapper m_ini;
};

#endif // TCOSCONFIGURATION_H
