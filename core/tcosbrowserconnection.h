#ifndef TCOSBROWSERCONNECTION_H
#define TCOSBROWSERCONNECTION_H
#include "tcosconnectionbase.h"

class TcosBrowserConnection : public TcosConnectionBase
{
public:
    TcosBrowserConnection(const QVariantMap &data = QVariantMap());

    // TcosConnectionBase interface
public:
    virtual ConnectionType type() const Q_DECL_OVERRIDE;
    virtual void writeConfiguration(QIODevice *device) const Q_DECL_OVERRIDE;
    virtual void writeLauncher(QIODevice *device, const QString &config) const Q_DECL_OVERRIDE;
};

#endif // TCOSBROWSERCONNECTION_H
