#include "linkfetcher.h"
#include <QNetworkAccessManager>
#include <QNetworkReply>
#include <QNetworkRequest>
#include <QUrl>
#include <QUrlQuery>

#include "errortypes.h"

LinkFetcher::LinkFetcher(QObject *parent) : QObject(parent)
{
    m_manager = new QNetworkAccessManager(this);
}

QPromise<QByteArray> LinkFetcher::fetch(const QUrl &fetchUrl)
{
    QNetworkRequest fetchRequest = QNetworkRequest(fetchUrl);

    fetchRequest.setHeader(QNetworkRequest::UserAgentHeader, QLatin1String("tcos/1.0"));
    fetchRequest.setAttribute(QNetworkRequest::FollowRedirectsAttribute, true);
    fetchRequest.setMaximumRedirectsAllowed(5);

    QNetworkReply *reply = m_manager->get(fetchRequest);

    return QPromise<QByteArray>(
        [this, reply, fetchRequest](const QPromiseResolve<QByteArray> &resolve, const QPromiseReject<QByteArray> &reject) {
            connect(reply, &QNetworkReply::finished, [resolve, reject, reply]() {
                if (reply->error() == QNetworkReply::NoError) {
                    return resolve(reply->readAll());
                } else {
                    NetworkReplyError e = {reply->error(),
                        QString(QLatin1String("Could not fetch %1")).arg(reply->url().toDisplayString())};
                    return reject(e);
                }
            });
        })
        .finally([reply](){
            reply->deleteLater();
        });
}

QPromise<void> LinkFetcher::fetch(const QUrl &fetchUrl, QIODevice *output)
{
    QNetworkRequest fetchRequest = QNetworkRequest(fetchUrl);

    fetchRequest.setHeader(QNetworkRequest::UserAgentHeader, QLatin1String("tcos/1.0"));
    fetchRequest.setAttribute(QNetworkRequest::FollowRedirectsAttribute, true);
    fetchRequest.setMaximumRedirectsAllowed(5);
    QNetworkReply *reply = m_manager->get(fetchRequest);

    return QPromise<void>(
        [this, reply, output, fetchRequest](const QPromiseResolve<void> &resolve, const QPromiseReject<void> &reject) {

            connect(reply, &QNetworkReply::finished, [resolve, reject, output, reply]() {
                if (reply->error() != QNetworkReply::NoError) {
                    NetworkReplyError e = {reply->error(),
                        QString(QLatin1String("Could not fetch %1")).arg(reply->url().toDisplayString())};
                    return reject(e);
                } else {
                    if (output->isOpen() && output->isWritable()) {
                        output->write(reply->readAll());
                    }
                    return resolve();
                }
            });
            connect(reply, &QNetworkReply::readyRead, [output, reply]() {
                if (output->isOpen() && output->isWritable()) {
                    output->write(reply->readAll());
                }
            });
        })
        .finally([reply](){
            reply->deleteLater();
        });
}
