#ifndef SYSTEMCONTROLLER_H
#define SYSTEMCONTROLLER_H

#include "tcosconnectionbase.h"

#include <QObject>
#include <QtPromise>

using namespace QtPromise;
class TcosConfiguration;

class SystemController : public QObject
{
    Q_OBJECT
public:
    explicit SystemController(QObject *parent = nullptr);
    QPromise<void> configure(const TcosConfiguration &configuration);
    QPromise<void> reboot();
    QPromise<bool> setHostname();

private:
    QPromise<void> setLanguage(const QString &lang);
    QPromise<void> setKeymap(const QString &layout);
    QPromise<void> setTimezone(const QString &timezone);
    QPromise<void> writeDpmsConfigureScript(int dpms);
    QPromise<void> writeAlsaConfigureScript(bool mute);
    QPromise<void> writeAutohideMouseConfigureScript(bool hide);
    QPromise<void> writeConnectionScript(const QSharedPointer<TcosConnectionBase> &connection);
};

#endif // SYSTEMCONTROLLER_H
