#ifndef UTILITIES_H
#define UTILITIES_H

#include <QtPromise>
#include <QString>
#include <QVariantMap>
#include <QDBusPendingCallWatcher>
#include <QDBusPendingReply>
#include "errortypes.h"

class Utilities
{
public:
    Utilities();
    static QString fetchMacAddress();
    static QString fetchMachine();
    static QString fetchDistro();
    static void setAtPath(const QStringList &path, QVariantMap *inputMap, const QVariant &value);
    static void setAtPath(const QString &path, QVariantMap *inputMap, const QVariant &value);
    static QVariantMap getAtPath(const QStringList &path, const QVariantMap &inputMap, const QVariant &defaultValue = QVariant());
    static QVariant getAtPath(const QString &path, const QVariantMap &inputMap, const QVariant &defaultValue = QVariant());

    static void mapPath(
            const QString &inputPath, const QVariantMap &input, const QString &outputPath, QVariantMap *output);

    template <typename T, typename R>
    static QtPromise::QPromise<T> dbusPromise(const QDBusPendingReply<R> & reply)
    {
        QDBusPendingCallWatcher *watcher = new QDBusPendingCallWatcher(reply);
        return QtPromise::QPromise<T>([watcher](const QtPromise::QPromiseResolve<T> &resolve, const QtPromise::QPromiseReject<T> &reject) {
            QObject::connect(watcher, &QDBusPendingCallWatcher::finished, [resolve, reject](QDBusPendingCallWatcher *w) {
                if (w->isError()) {
                    DbusFailure e = {w->error(), QLatin1String("DBus Error")};
                    return reject(e);
                } else {
                    QDBusPendingReply<R> reply = *w;
                    return resolve(reply.value());
                }
            });
        })
        .finally([watcher]() {
            watcher->deleteLater();
        });
    }

    template <typename R>
    static QtPromise::QPromise<void> dbusPromise(const QDBusPendingReply<R> & reply)
    {
        QDBusPendingCallWatcher *watcher = new QDBusPendingCallWatcher(reply);
        return QtPromise::QPromise<void>([watcher](const QtPromise::QPromiseResolve<void> &resolve, const QtPromise::QPromiseReject<void> &reject) {
            QObject::connect(watcher, &QDBusPendingCallWatcher::finished, [resolve, reject](QDBusPendingCallWatcher *w) {
                if (w->isError()) {
                    DbusFailure e = {w->error(), QLatin1String("DBus Error")};
                    return reject(e);
                } else {
                    return resolve();
                }
            });
        })
        .finally([watcher]() {
            watcher->deleteLater();
        });
    }
};

#endif // UTILITIES_H
