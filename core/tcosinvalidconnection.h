#ifndef TCOSINVALIDCONNECTION_H
#define TCOSINVALIDCONNECTION_H

#include "tcosconnectionbase.h"

class TcosInvalidConnection : public TcosConnectionBase
{
public:
    TcosInvalidConnection(const QVariantMap &data = QVariantMap());

public:
    virtual ConnectionType type() const Q_DECL_OVERRIDE;
    virtual void writeConfiguration(QIODevice *device) const Q_DECL_OVERRIDE;
    virtual void writeLauncher(QIODevice *device, const QString &config) const Q_DECL_OVERRIDE;
};

#endif // TCOSINVALIDCONNECTION_H
