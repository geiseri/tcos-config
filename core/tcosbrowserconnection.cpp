#include "tcosbrowserconnection.h"
#include "utilities.h"

#include <QJsonDocument>
#include <QJsonObject>
#include <QTextStream>

TcosBrowserConnection::TcosBrowserConnection(const QVariantMap &data) : TcosConnectionBase(data)
{
}

TcosConnectionBase::ConnectionType TcosBrowserConnection::type() const
{
    return BROWSER;
}

void TcosBrowserConnection::writeConfiguration(QIODevice *device) const
{
    QVariantMap browserConfig;

    // Defaults
    Utilities::setAtPath(
        QLatin1String("GlobalSettings/Browser/WhitelistPermissions"), &browserConfig, QVariant::fromValue(true));

    // settings
    Utilities::mapPath(
        QLatin1String("start_url"), configuration(), QLatin1String("GlobalSettings/Browser/StartUrl"), &browserConfig);
    Utilities::mapPath(QLatin1String("whitelist_permissions"), configuration(),
        QLatin1String("GlobalSettings/Browser/WhitelistPermissions"), &browserConfig);

    // Permissions
    Utilities::mapPath(QLatin1String("permissions/allow_back_button"), configuration(),
        QLatin1String("GlobalSettings/Access/BrowserFeature::AllowBackButton"), &browserConfig);
    Utilities::mapPath(QLatin1String("permissions/allow_reload_button"), configuration(),
        QLatin1String("GlobalSettings/Access/BrowserFeature::AllowReloadButton"), &browserConfig);
    Utilities::mapPath(QLatin1String("permissions/allow_url_edit"), configuration(),
        QLatin1String("GlobalSettings/Access/BrowserFeature::AllowUrlEdit"), &browserConfig);
    Utilities::mapPath(QLatin1String("permissions/allow_browser_close"), configuration(),
        QLatin1String("GlobalSettings/Access/BrowserFeature::AllowBrowserClose"), &browserConfig);
    Utilities::mapPath(QLatin1String("permissions/allow_create_tabs"), configuration(),
        QLatin1String("GlobalSettings/Access/BrowserFeature::AllowCreateTabs"), &browserConfig);
    Utilities::mapPath(QLatin1String("permissions/allow_fullscreen"), configuration(),
        QLatin1String("GlobalSettings/Access/BrowserFeature::AllowFullscreen"), &browserConfig);
    Utilities::mapPath(QLatin1String("permissions/allow_uploads"), configuration(),
        QLatin1String("GlobalSettings/Access/BrowserFeature::AllowUploads"), &browserConfig);
    Utilities::mapPath(QLatin1String("permissions/allow_downloads"), configuration(),
        QLatin1String("GlobalSettings/Access/BrowserFeature::AllowDownloads"), &browserConfig);
    Utilities::mapPath(QLatin1String("permissions/allow_html5_notifications"), configuration(),
        QLatin1String("GlobalSettings/Access/WebFeature::Notifications"), &browserConfig);
    Utilities::mapPath(QLatin1String("permissions/allow_html5_geolocation"), configuration(),
        QLatin1String("GlobalSettings/Access/WebFeature::Geolocation"), &browserConfig);
    Utilities::mapPath(QLatin1String("permissions/allow_html5_media_audio_capture"), configuration(),
        QLatin1String("GlobalSettings/Access/WebFeature::MediaAudioCapture"), &browserConfig);
    Utilities::mapPath(QLatin1String("permissions/allow_html5_media_video_capture"), configuration(),
        QLatin1String("GlobalSettings/Access/WebFeature::MediaVideoCapture"), &browserConfig);
    Utilities::mapPath(QLatin1String("permissions/allow_html5_media_audio_video_capture"), configuration(),
        QLatin1String("GlobalSettings/Access/WebFeature::MediaAudioVideoCapture"), &browserConfig);
    Utilities::mapPath(QLatin1String("permissions/allow_html5_mouse_lock"), configuration(),
        QLatin1String("GlobalSettings/Access/WebFeature::MouseLock"), &browserConfig);
    Utilities::mapPath(QLatin1String("permissions/allow_ssl_invalid_certificate_authority"), configuration(),
        QLatin1String("GlobalSettings/Access/SSLError::CertificateAuthorityInvalid"), &browserConfig);
    // Utilities::mapPath(QLatin1String("permissions/allow_..."),configuration(),
    // QLatin1String("GlobalSettings/Access/WebAction::..."), &browserConfig);

    // Mime handling
    QVariantList mimeList;

    if (Utilities::getAtPath(QLatin1String("external/rdp"), configuration()).toBool()) {
        QVariantMap rdp;
        rdp[QLatin1String("Type")] = QLatin1String("application/x-rdp");
        rdp[QLatin1String("Runner")] = QLatin1String("/usr/bin/rdpwrapper");
        mimeList << rdp;
    }

    if (Utilities::getAtPath(QLatin1String("external/pdf"), configuration()).toBool()) {
        QVariantMap pdf;
        pdf[QLatin1String("Type")] = QLatin1String("application/pdf");
        pdf[QLatin1String("Runner")] = QLatin1String("/usr/bin/mupdf");
        mimeList << pdf;
    }

    if (!mimeList.isEmpty()) {
        Utilities::setAtPath(QLatin1String("GlobalSettings/Access/BrowserFeature::AllowDownloads"), &browserConfig,
            QVariant::fromValue(true));
        Utilities::setAtPath(QLatin1String("GlobalSettings/MimeTypes"), &browserConfig, QVariant::fromValue(mimeList));
    }

    QJsonDocument document;
    document.setObject(QJsonObject::fromVariantMap(browserConfig));
    device->write(document.toJson());
}

void TcosBrowserConnection::writeLauncher(QIODevice *device, const QString &config) const
{
    QTextStream output(device);
    output << QLatin1String("#!/bin/sh\n");
    output << QLatin1String("/usr/bin/x-window-manager&\n");
    output << QLatin1String("/usr/bin/KioskBrowser ") << config << QLatin1String("\n");
}
