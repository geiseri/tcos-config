#ifndef LINKFETCHER_H
#define LINKFETCHER_H

#include <QObject>
#include <QtPromise>

using namespace QtPromise;

class QNetworkAccessManager;
class QIODevice;

class LinkFetcher : public QObject
{
    Q_OBJECT
public:
    explicit LinkFetcher(QObject *parent = nullptr);
    QPromise<QByteArray> fetch(const QUrl &fetchUrl);
    QPromise<void> fetch(const QUrl &fetchUrl, QIODevice *output);

private:
    QNetworkAccessManager *m_manager;
};

#endif // LINKFETCHER_H
