#include "tcosinvalidconnection.h"
#include <QTextStream>

TcosInvalidConnection::TcosInvalidConnection(const QVariantMap &data) : TcosConnectionBase(data)
{
}

TcosConnectionBase::ConnectionType TcosInvalidConnection::type() const
{
    return INVALID;
}

void TcosInvalidConnection::writeConfiguration(QIODevice *device) const
{
    Q_UNUSED(device);
}

void TcosInvalidConnection::writeLauncher(QIODevice *device, const QString &config) const
{
    Q_UNUSED(config);
    QTextStream output(device);
    output << QLatin1String("#!/bin/sh\n");
    output << QLatin1String("/usr/bin/x-window-manager&\n");
    output << QLatin1String("/usr/bin/matchbox-terminal\n");
}
