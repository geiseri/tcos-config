#include "utilities.h"
#include <QDebug>
#include <QFile>
#include <QNetworkInterface>

Utilities::Utilities()
{
}

QString Utilities::fetchMacAddress()
{
    for (QNetworkInterface iface : QNetworkInterface::allInterfaces()) {
        if (!iface.flags().testFlag(QNetworkInterface::IsLoopBack) && iface.name().startsWith(QLatin1String("en"))) {
            return iface.hardwareAddress().replace(QLatin1Char(':'), QLatin1String("-"));
        }
    }
    return QLatin1String("<none>");
}

QString Utilities::fetchMachine()
{
    QFile machine(QLatin1String("/run/initramfs/image/machine.txt"));
    if (machine.open(QIODevice::ReadOnly)) {
        return machine.readAll().trimmed();
    } else {
        return QLatin1String("<none>");
    }
}

QString Utilities::fetchDistro()
{
    QFile distro(QLatin1String("/run/initramfs/image/distro.txt"));
    if (distro.open(QIODevice::ReadOnly)) {
        return distro.readAll().trimmed();
    } else {
        return QLatin1String("<none>");
    }
}

void Utilities::setAtPath(const QStringList &path, QVariantMap *inputMap, const QVariant &value)
{
    if (path.first() == path.last()) {
        (*inputMap)[path.first()] = value;
    } else if (inputMap->contains(path.first())) {
        QStringList newPath = path;
        newPath.removeFirst();
        QVariantMap newMap = (*inputMap)[path.first()].toMap();
        setAtPath(newPath, &newMap, value);
        (*inputMap)[path.first()] = newMap;
    } else {
        QStringList newPath = path;
        newPath.removeFirst();
        QVariantMap newMap;
        setAtPath(newPath, &newMap, value);
        (*inputMap)[path.first()] = newMap;
    }
}

void Utilities::setAtPath(const QString &path, QVariantMap *inputMap, const QVariant &value)
{
    QStringList pathParts = path.split(QLatin1Char('/'), QString::SkipEmptyParts);
    setAtPath(pathParts, inputMap, value);
}

QVariantMap Utilities::getAtPath(const QStringList &path, const QVariantMap &inputMap, const QVariant &defaultValue)
{
    if (!path.isEmpty() && inputMap.contains(path.first())) {
        QStringList newPath = path;
        newPath.removeFirst();
        QVariantMap newMap = inputMap[path.first()].toMap();
        return getAtPath(newPath, newMap, defaultValue);
    } else {
        return inputMap;
    }
}

QVariant Utilities::getAtPath(const QString &path, const QVariantMap &inputMap, const QVariant &defaultValue)
{
    QStringList pathParts = path.split(QLatin1Char('/'), QString::SkipEmptyParts);
    if (!pathParts.isEmpty()) {
        QString lastPart = pathParts.takeLast();
        return getAtPath(pathParts, inputMap, defaultValue)[lastPart];
    } else {
        return defaultValue;
    }
}

void Utilities::mapPath(
    const QString &inputPath, const QVariantMap &input, const QString &outputPath, QVariantMap *output)
{
    QVariant inputValue = getAtPath(inputPath, input);
    if (inputValue.isValid()) {
        setAtPath(outputPath, output, inputValue);
    }
}
