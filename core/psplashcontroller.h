#ifndef PSPLASHCONTROLLER_H
#define PSPLASHCONTROLLER_H

#include <QObject>

class PSplashController : public QObject
{
    Q_OBJECT
public:
    explicit PSplashController(QObject *parent = nullptr);

public slots:
    void start();
    void writeMessage(const QString &message, uint progress);
    void setProgress(uint progress);
    void quit();
};

#endif // PSPLASHCONTROLLER_H
