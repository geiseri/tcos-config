#ifndef ERRORTYPES_H
#define ERRORTYPES_H
#include <QDBusError>
#include <QDebug>
#include <QDnsLookup>
#include <QFileDevice>
#include <QNetworkReply>
#include <QProcess>
#include <QString>

template<typename T>
struct BaseError {
    T error;
    QString message;
};

template<typename T>
QDebug operator<<(QDebug stream, const BaseError<T> &error)
{
    QDebugStateSaver saver(stream);
    stream << error.message << error.error;
    return stream;
}

typedef BaseError<QProcess::ProcessError> ProcessError;
typedef BaseError<QNetworkReply::NetworkError> NetworkReplyError;
typedef BaseError<QDnsLookup::Error> DNSError;
typedef BaseError<QFileDevice::FileError> FileError;
typedef BaseError<QDBusError> DbusFailure;
typedef BaseError<int> ConfigurationError;

#endif // ERRORTYPES_H
