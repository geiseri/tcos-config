#include "tcosconfiguration.h"
#include "tcosbrowserconnection.h"
#include "tcosrdpconnection.h"
#include <QDebug>
#include <QJsonDocument>
#include <QJsonObject>
#include <QVariant>

TcosConfiguration::TcosConfiguration() : m_isValid(false)
{
}

bool TcosConfiguration::parseConfiguration(const QByteArray &configuration)
{
    m_isValid = m_ini.parse(configuration);
    return m_isValid;
}

QString TcosConfiguration::timezone(const QString &defaultValue) const
{
    return m_ini.value(QLatin1String("locale"), QLatin1String("timezone"), defaultValue);
}

QString TcosConfiguration::keymap(const QString &defaultValue) const
{
    return m_ini.value(QLatin1String("hardware"), QLatin1String("keymap"), defaultValue);
}

QString TcosConfiguration::language(const QString &defaultValue) const
{
    return m_ini.value(QLatin1String("locale"), QLatin1String("language"), defaultValue);
}

int TcosConfiguration::dpms(quint8 defaultValue) const
{
    return m_ini.value(QLatin1String("hardware"), QLatin1String("dpms"), defaultValue);
}

bool TcosConfiguration::mute(bool defaultValue) const
{
    return m_ini.value(QLatin1String("hardware"), QLatin1String("sound"), defaultValue);
}

bool TcosConfiguration::autohideMouse(bool defaultValue) const
{
    return m_ini.value(QLatin1String("hardware"), QLatin1String("autohide_mouse"), defaultValue);
}

QSharedPointer<TcosConnectionBase> TcosConfiguration::connection() const
{
    return TcosConnectionBase::factory(m_ini.section(QLatin1String("connection")));
}

FirmwareConfiguration TcosConfiguration::firmware() const
{
    return FirmwareConfiguration(m_ini.section(QLatin1String("firmware")));
}

QUrl TcosConfiguration::baseUrl() const
{
    return m_baseUrl;
}

void TcosConfiguration::setBaseUrl(const QUrl &baseUrl)
{
    m_baseUrl = baseUrl;
}

bool TcosConfiguration::isValid() const
{
    return m_isValid;
}
