#ifndef TCOSRDPCONNECTION_H
#define TCOSRDPCONNECTION_H

#include "tcosconnectionbase.h"

class TcosRdpConnection : public TcosConnectionBase
{
public:
    TcosRdpConnection(const QVariantMap &data = QVariantMap());

public:
    virtual ConnectionType type() const Q_DECL_OVERRIDE;
    virtual void writeConfiguration(QIODevice *device) const Q_DECL_OVERRIDE;
    virtual void writeLauncher(QIODevice *device, const QString &config) const Q_DECL_OVERRIDE;
};

#endif // TCOSRDPCONNECTION_H
