#ifndef FIRMWARECONFIGURATION_H
#define FIRMWARECONFIGURATION_H

#include <QVariant>

class QUrl;
class FirmwareConfiguration
{
public:
    FirmwareConfiguration(const QVariantMap &data = QVariantMap());
    bool isValid() const;

    QString md5sum() const;
    QUrl url() const;

private:
    QVariantMap m_configuration;
};

#endif // FIRMWARECONFIGURATION_H
