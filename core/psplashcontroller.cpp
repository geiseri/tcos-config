#include "psplashcontroller.h"
#include <QDebug>
#include <QProcess>

static QString psplash = QLatin1String("/usr/bin/psplash");
static QString psplash_write = QLatin1String("/usr/bin/psplash-write");
static QString messageTemplate = QLatin1String("MSG %1");
static QString progressTemplate = QLatin1String("PROGRESS %1");

PSplashController::PSplashController(QObject *parent) : QObject(parent)
{
}

void PSplashController::start()
{
    QProcess::startDetached(psplash);
}

void PSplashController::writeMessage(const QString &message, uint progress)
{
    qDebug() << Q_FUNC_INFO << messageTemplate.arg(message);
    QProcess::startDetached(psplash_write, {messageTemplate.arg(message)});
    setProgress(progress);
}

void PSplashController::setProgress(uint progress)
{
    QProcess::startDetached(psplash_write, {progressTemplate.arg(progress)});
}

void PSplashController::quit()
{
    QProcess::startDetached(psplash_write, {QLatin1String("QUIT")});
}
