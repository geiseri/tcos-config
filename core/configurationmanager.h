#ifndef CONFIGURATIONMANAGER_H
#define CONFIGURATIONMANAGER_H

#include "tcosconfiguration.h"
#include <QObject>
#include <QtPromise>

using namespace QtPromise;

class StateTracker;
class LinkFetcher;
class LinkResolver;
class SystemController;
class PSplashController;

class ConfigurationManager : public QObject
{
    Q_OBJECT
public:
    explicit ConfigurationManager(QObject *parent = nullptr);

signals:
    void gotUrls(const QStringList &urls);
    void finished();

public slots:

private slots:
    void handleNetwortOnline();
    void handleNetworkOffline();

private:
    QPromise<TcosConfiguration> getConfiguration(const QUrl &url) const;
    QPromise<TcosConfiguration> getConfigurations(const QList<QUrl> &urls) const;
    QPromise<void> handleFirmware(const TcosConfiguration &config);
    QPromise<QString> fetchFirmware(const TcosConfiguration &config) const;
    QPromise<void> updateFirmware(const TcosConfiguration &config, const QString &path) const;
    bool checkFirmwareUpdate(const QString &md5sum) const;

    StateTracker *m_tracker;
    LinkFetcher *m_fetcher;
    LinkResolver *m_resolver;
    SystemController *m_controller;
    PSplashController *m_psplash;
};

#endif // CONFIGURATIONMANAGER_H
