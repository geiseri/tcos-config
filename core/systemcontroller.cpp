#include "systemcontroller.h"
#include <QDir>
#include <QFile>
#include <QTemporaryFile>
#include <QTextStream>
#include <localed/localed.h>
#include <timedated/timedate.h>
#include <logind/manager.h>
#include <hostnamed/hostnamed.h>

#include "tcosconfiguration.h"
#include "tcosconnectionbase.h"
#include "utilities.h"

#include "errortypes.h"

SystemController::SystemController(QObject *parent) : QObject(parent)
{
}

QPromise<void> SystemController::configure(const TcosConfiguration &configuration)
{
    return QPromise<void>::all(QVector<QPromise<void>>({
        setLanguage(configuration.language(QLatin1String("en_US"))),
        setTimezone(configuration.timezone(QLatin1String("US/Eastern"))),
        setKeymap(configuration.keymap(QLatin1String("us"))),
        writeAlsaConfigureScript(configuration.mute(false)),
        writeDpmsConfigureScript(configuration.dpms(15)),
        writeAutohideMouseConfigureScript(configuration.autohideMouse(false)),
        writeConnectionScript(configuration.connection())}));
}

QPromise<void> SystemController::reboot()
{
    Logind::Manager *mgr = new Logind::Manager();
    return Utilities::dbusPromise(mgr->interface<org::freedesktop::login1::Manager>()->Reboot(false))
    .fail([](const DbusFailure failure){
        throw DbusFailure({failure.error,  QLatin1String("Error rebooting")});
    })
    .finally([mgr]() {
        delete mgr;
    });
}

QPromise<bool> SystemController::setHostname()
{
    Hostnamed::Hostname *hostnamed = new Hostnamed::Hostname();
    QString hostname = Utilities::fetchMacAddress();
    return QPromise<void>::all(QVector<QPromise<void>>({
        Utilities::dbusPromise(hostnamed->interface<org::freedesktop::hostname1>()->SetPrettyHostname(hostname, false)),
        Utilities::dbusPromise(hostnamed->interface<org::freedesktop::hostname1>()->SetStaticHostname(hostname, false)),
        Utilities::dbusPromise(hostnamed->interface<org::freedesktop::hostname1>()->SetHostname(hostname, false))
    }))
    .then([](){
        return true;
    })
    .fail([](){
        return false;
    })
    .finally([hostnamed](){
        delete hostnamed;
    });
}

QPromise<void> SystemController::setLanguage(const QString &lang)
{
    Localed::Locale *locale = new Localed::Locale();
    QStringList currentSettings = locale->interface<org::freedesktop::locale1>()->locale();
    // LANG=, LC_CTYPE=, LC_NUMERIC=, LC_TIME=, LC_COLLATE=, LC_MONETARY=, LC_MESSAGES=, LC_PAPER=, LC_NAME=,
    // LC_ADDRESS=, LC_TELEPHONE=, LC_MEASUREMENT=, LC_IDENTIFICATION=
    QString langTemplate = QLatin1String("LANG=%1");

    return Utilities::dbusPromise(locale->interface<org::freedesktop::locale1>()->SetLocale({langTemplate.arg(lang)}, false))
    .fail([](const DbusFailure failure){
        throw DbusFailure({failure.error,  QLatin1String("Error setting locale")});
    })
    .finally([locale]() {
        delete locale;
    });
}

QPromise<void> SystemController::setKeymap(const QString &layout)
{
    Localed::Locale *locale = new Localed::Locale();

    return Utilities::dbusPromise(locale->interface<org::freedesktop::locale1>()->SetX11Keyboard(layout, QString(), /* model */
                                                                                      QString(), /* variantl */
                                                                                      QString(), /* options l */
                                                                                      false, false))
    .fail([](const DbusFailure failure){
        throw DbusFailure({failure.error, QLatin1String("Error setting keymap")});
    })
    .finally([locale]() {
        delete locale;
    });
}

QPromise<void> SystemController::setTimezone(const QString &timezone)
{
    TimeDated::TimeDate *timedate = new TimeDated::TimeDate();
    return Utilities::dbusPromise(timedate->interface<org::freedesktop::timedate1>()->SetTimezone(timezone, false))
    .fail([](const DbusFailure failure){
        throw DbusFailure({failure.error, QLatin1String("Error setting timezone")});
    })
    .finally([timedate]() {
        delete timedate;
    });
}

QPromise<void> SystemController::writeDpmsConfigureScript(int dpms)
{
    return QPromise<void>([dpms](const QPromiseResolve<void> &resolve, const QPromiseReject<void> &reject) {
        QFile script(QLatin1String("/etc/X11/Xsession.d/50-dpms"));
        if (script.open(QIODevice::WriteOnly)) {
            QTextStream output(&script);
            output << QLatin1String("#!/bin/sh\n");
            if (dpms == 0) {
                output << QLatin1String("xset s off -dpms\n");
            } else {
                output << QLatin1String("xset s off dpms ")
                       << (dpms * 60) << QLatin1Char(' ')
                       << (dpms * 60) << QLatin1Char(' ')
                       << (dpms * 60) << QLatin1Char('\n');
            }
            script.close();
        } else {
            FileError e = {script.error(), QLatin1String("Error writing dpms settings")};
            return reject(e);
        }

        script.setPermissions(QFile::ExeOther | QFile::ExeGroup | QFile::ExeOwner | QFile::ReadOther);
        return resolve();
    });
}

QPromise<void> SystemController::writeAlsaConfigureScript(bool mute)
{
    return QPromise<void>([mute](const QPromiseResolve<void> &resolve, const QPromiseReject<void> &reject) {
        QFile script(QLatin1String("/etc/X11/Xsession.d/50-mute"));
        if (script.open(QIODevice::WriteOnly)) {
            QTextStream output(&script);
            output << QLatin1String("#!/bin/sh\n");
            if (mute) {
                output << QLatin1String("amixer sset Master mute\n");
            } else {
                output << QLatin1String("amixer sset Master unmute\n");
            }
        } else {
            FileError e = {script.error(), QLatin1String("Error writing alsa settings")};
            return reject(e);
        }

        script.setPermissions(QFile::ExeOther | QFile::ExeGroup | QFile::ExeOwner | QFile::ReadOther);
        return resolve();
    });
}

QPromise<void> SystemController::writeAutohideMouseConfigureScript(bool hide)
{
    return QPromise<void>([hide](const QPromiseResolve<void> &resolve, const QPromiseReject<void> &reject) {
        QFile script(QLatin1String("/etc/X11/Xsession.d/50-hidemouse"));
        if (script.open(QIODevice::WriteOnly)) {
            QTextStream output(&script);
            output << QLatin1String("#!/bin/sh\n");
            if (hide) {
                output << QLatin1String("unclutter&\n");
            } else {
                output << QLatin1String("\n");
            }
        } else {
            FileError e = {script.error(), QLatin1String("Error writing mouse settings")};
            return reject(e);
        }

        script.setPermissions(QFile::ExeOther | QFile::ExeGroup | QFile::ExeOwner | QFile::ReadOther);
        return resolve();
    });
}

QPromise<void> SystemController::writeConnectionScript(const QSharedPointer<TcosConnectionBase> &connection)
{
    return QPromise<void>([connection](const QPromiseResolve<void> &resolve, const QPromiseReject<void> &reject) {
        if (connection->isValid()) {
            QFile launchFile(QLatin1String("/usr/bin/x-session-manager"));
            QTemporaryFile configurationFile;
            configurationFile.setAutoRemove(false);
            configurationFile.setFileTemplate(QLatin1String("/tmp/connection-XXXXXX.json"));

            if (launchFile.open(QIODevice::WriteOnly)) {
                if (configurationFile.open()) {
                    connection->writeConfiguration(&configurationFile);
                    connection->writeLauncher(&launchFile, configurationFile.fileName());
                    launchFile.setPermissions(QFile::ExeOther | QFile::ExeGroup | QFile::ExeOwner | QFile::ReadOther);
                    configurationFile.setPermissions(QFile::ReadOwner | QFile::ReadGroup | QFile::ReadOther);
                    return resolve();
                } else {
                    FileError e = {configurationFile.error(),
                        QLatin1String("Error writing connection configuration: ") + configurationFile.errorString()};
                    return reject(e);
                }
            } else {
                FileError e = {launchFile.error(),
                    QLatin1String("Error setting up x-session-manager: ") + launchFile.errorString()};
                return reject(e);
            }

        } else {
            qDebug() << Q_FUNC_INFO << "invalid connection";
            return reject(QLatin1String("Invalid connection"));
        }
    });
}
