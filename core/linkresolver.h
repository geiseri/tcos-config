#ifndef LINKRESOLVER_H
#define LINKRESOLVER_H

#include <QList>
#include <QObject>
#include <QUrl>
#include <QtPromise>

using namespace QtPromise;

class LinkResolver : public QObject
{
    Q_OBJECT
public:
    explicit LinkResolver(QObject *parent = nullptr);

    QPromise<QList<QUrl>> resolve(const QString &service);
};

#endif // LINKRESOLVER_H
