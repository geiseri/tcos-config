#ifndef TCOSCONNECTIONBASE_H
#define TCOSCONNECTIONBASE_H

#include <QSharedPointer>
#include <QVariantMap>

class QString;
class QIODevice;

class TcosConnectionBase
{
public:
    enum ConnectionType { INVALID, RDP, BROWSER };
    TcosConnectionBase(const QVariantMap &data = QVariantMap());
    virtual ~TcosConnectionBase() {;}

    bool isValid() const;
    virtual ConnectionType type() const = 0;
    virtual void writeConfiguration(QIODevice *device) const = 0;
    virtual void writeLauncher(QIODevice *device, const QString &config) const = 0;

    static QSharedPointer<TcosConnectionBase> factory(const QVariantMap &data = QVariantMap());

protected:
    virtual QVariantMap configuration() const;

private:
    QVariantMap m_configuration;
};

#endif // TCOSCONNECTIONBASE_H
