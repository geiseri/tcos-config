#include "firmwareconfiguration.h"
#include <QUrl>

FirmwareConfiguration::FirmwareConfiguration(const QVariantMap &data) : m_configuration(data)
{
}

bool FirmwareConfiguration::isValid() const
{
    if (m_configuration.isEmpty()) {
        return false;
    }

    if (md5sum().isEmpty()) {
        return false;
    }

    if (!url().isValid()) {
        return false;
    }

    return true;
}

QString FirmwareConfiguration::md5sum() const
{
    return m_configuration[QLatin1String("md5sum")].toString();
}

QUrl FirmwareConfiguration::url() const
{
    return QUrl(m_configuration[QLatin1String("url")].toString());
}
