#ifndef INIHWRAPPER_H
#define INIHWRAPPER_H

#include <QMap>
#include <QString>
#include <QVariant>

class InihReaderWrapper
{

public:
    InihReaderWrapper();

    bool parse(const QByteArray &buffer);
    QVariantMap section(const QString &name) const;

    template <typename T>
    T value(const QString &section, const QString &key, const T &defaultValue) const
    {
        return qvariant_cast<T>(valueInternal(section, key, QVariant::fromValue(defaultValue)));
    }

private:
    QVariant valueInternal(const QString &section, const QString &key, const QVariant &defaultValue) const;

    static int line_handler(void *user, const char *section, const char *name, const char *value, int lineno);

    QVariantMap m_settings;
};

#endif // INIHWRAPPER_H
