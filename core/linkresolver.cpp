#include "linkresolver.h"
#include <QDnsLookup>
#include <QHostAddress>
#include <QHostInfo>

#include "errortypes.h"
#include "utilities.h"

LinkResolver::LinkResolver(QObject *parent) : QObject(parent)
{
}

QPromise<QList<QUrl>> LinkResolver::resolve(const QString &service)
{
    QString name = service + QLatin1Char('.') + QHostInfo::localDomainName();
    QDnsLookup *dns = new QDnsLookup(QDnsLookup::SRV, name);
    return QPromise<QList<QUrl>>(
        [dns](const QPromiseResolve<QList<QUrl>> &resolve, const QPromiseReject<QList<QUrl>> &reject) {
            QObject::connect(dns, &QDnsLookup::finished, [dns, resolve, reject]() {
                if (dns->error() != QDnsLookup::NoError) {
                    DNSError e
                        = {dns->error(), QString(QLatin1String("Could not fetch SRV record for %1")).arg(dns->name())};
                    return reject(e);
                } else {
                    const auto records = dns->serviceRecords();
                    QList<QUrl> results;
                    for (const QDnsServiceRecord &record : records) {
                        QUrl url;
                        url.setScheme(QLatin1String("http"));
                        url.setHost(record.target());
                        url.setPort(record.port());
                        results << url;
                    }
                    return resolve(results);
                }
            });
            dns->lookup();
        })
        .finally([dns](){
            dns->deleteLater();
        });
}
