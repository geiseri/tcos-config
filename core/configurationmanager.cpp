#include "configurationmanager.h"
#include "errortypes.h"
#include "linkfetcher.h"
#include "linkresolver.h"
#include "psplashcontroller.h"
#include "systemcontroller.h"
#include "tcosconfiguration.h"
#include "utilities.h"

#include <QDBusError>
#include <QDebug>
#include <QDnsLookup>
#include <QFileDevice>
#include <QNetworkReply>
#include <QProcess>
#include <QTemporaryFile>
#include <qtsystemd/statetracker.h>

ConfigurationManager::ConfigurationManager(QObject *parent) : QObject(parent)
{
    m_psplash = new PSplashController(this);
    m_fetcher = new LinkFetcher(this);
    m_controller = new SystemController(this);
    m_resolver = new LinkResolver(this);

    m_controller->setHostname();

    QSharedPointer<Systemd::Manager> mgr(new Systemd::Manager(QDBusConnection::systemBus()));
    m_tracker = new StateTracker(mgr, this);
    connect(m_tracker, &StateTracker::unitStarted, this, &ConfigurationManager::handleNetwortOnline);
    connect(m_tracker, &StateTracker::unitStopped, this, &ConfigurationManager::handleNetworkOffline);

    m_tracker->track(QLatin1String("network-online.target"));
    m_tracker->start();

    m_psplash->start();
    m_psplash->writeMessage(QLatin1String("Waiting for network..."), 25);
}

QPromise<void> ConfigurationManager::handleFirmware(const TcosConfiguration &config)
{
    return m_controller->configure(config)
        .then([this, config]() { return fetchFirmware(config); })
        .then([this, config](const QString &firmwarePath) {
            m_psplash->writeMessage(QLatin1String("Updating firmware..."), 90);
            return updateFirmware(config, firmwarePath);
        });
}

void ConfigurationManager::handleNetwortOnline()
{
    m_psplash->writeMessage(QLatin1String("Searching for configurations..."), 50);
    m_resolver->resolve(QLatin1String("_tcos._tcp"))
        .then([this](const QList<QUrl> &addresses) {
            m_psplash->writeMessage(QLatin1String("Fetching configurations..."), 60);
            return getConfigurations(addresses);
        })
        .then([this](const TcosConfiguration &config) {
            m_psplash->writeMessage(QLatin1String("Configuring client..."), 70);
            return handleFirmware(config);
        })
        .then([this](){
            m_psplash->writeMessage(QLatin1String("Starting Session..."), 100);
            Q_EMIT finished();
        })
        .fail([](DbusFailure error) { qWarning() << "DBus Error:" << error; })
        .fail([](FileError error) { qWarning() << "Filesystem Error:" << error; })
        .fail([](DNSError error) { qWarning() << "Resolver Error:" << error; })
        .fail([](NetworkReplyError error) { qWarning() << "Fetch Error:" << error; })
        .fail([](ProcessError error) { qWarning() << "Process Error:" << error; })
        .fail([](ConfigurationError error) { qWarning() << "Configuration Error:" << error; })
        .fail([](const QString &error) { qWarning() << "Error:" << error; })
        .fail([](const QLatin1String &error) { qWarning() << "Error:" << error; })
        .fail([]() { qWarning() << "Unknown Error"; });
}

void ConfigurationManager::handleNetworkOffline()
{
}

QPromise<TcosConfiguration> ConfigurationManager::getConfiguration(const QUrl &url) const
{
    return m_fetcher->fetch(url)
    .then([url](const QByteArray &configData) {
        TcosConfiguration config;
        if (config.parseConfiguration(configData)) {
            config.setBaseUrl(url);
            return config;
        } else {
            return TcosConfiguration();
        }
    })
    .fail([url](NetworkReplyError error) {
        if (error.error == QNetworkReply::ContentNotFoundError ||
            error.error == QNetworkReply::ConnectionRefusedError ||
            error.error == QNetworkReply::HostNotFoundError) {
            return TcosConfiguration();
        }
        throw NetworkReplyError({error.error, QString(QLatin1String("Error fetching %1")).arg(url.toDisplayString())});
    });
}

QPromise<TcosConfiguration> ConfigurationManager::getConfigurations(const QList<QUrl> &urls) const
{

    if (urls.count() == 0) {
        return QPromise<TcosConfiguration>::reject(ConfigurationError({0, QLatin1String("No configuration urls")}));
    }

    QList<QPromise<TcosConfiguration>> configurationPromises;

    for (QUrl url : urls) {
        configurationPromises.push_back(getConfiguration((url.resolved(Utilities::fetchMacAddress() + QLatin1String(".ini")))));
        configurationPromises.push_back(getConfiguration((url.resolved(Utilities::fetchMachine() + QLatin1String(".ini")))));
        configurationPromises.push_back(getConfiguration((url.resolved(Utilities::fetchDistro() + QLatin1String(".ini")))));
    }

    // Fallbacks
    for (QUrl url : urls) {
        configurationPromises.push_back(getConfiguration((url.resolved(QUrl(QLatin1String("tcos.ini"))))));
    }

    return QPromise<TcosConfiguration>::all(configurationPromises)
    .then([](const QVector<TcosConfiguration>& configurations) {
        for( TcosConfiguration configuration : configurations ) {
            if (configuration.isValid()) {return configuration;}
        }
        throw ConfigurationError({1, QLatin1String("No useable configurations")});
    });
}

QPromise<QTemporaryFile*> getTempFile()
{
    QTemporaryFile* firmware = new QTemporaryFile();
    firmware->setAutoRemove(false);
    return QPromise<QTemporaryFile*>([firmware](const QPromiseResolve<QTemporaryFile*> &resolve,
                                                const QPromiseReject<QTemporaryFile*> &reject) {
        if (firmware->open()) {
            return resolve(firmware);
        } else {
            return reject(FileError{firmware->error(), QLatin1String("Temporary file write error")});
        }
    })
    .finally([firmware](){
        firmware->deleteLater();
    });
}

QPromise<QString> ConfigurationManager::fetchFirmware(const TcosConfiguration &config) const
{
    if (config.firmware().isValid() && !checkFirmwareUpdate(config.firmware().md5sum())) {
        m_psplash->writeMessage(QLatin1String("Downloading firmware..."), 80);
        return getTempFile().then([this, config](QTemporaryFile *firmware){
            QUrl url = config.firmware().url();
            if (url.scheme().isEmpty()) {
                url = config.baseUrl().resolved(url);
            }
            return m_fetcher->fetch(url, firmware)
                    .then([firmware]() { return firmware->fileName(); });
        });
    } else {
        m_psplash->writeMessage(QLatin1String("Firmware up to date..."), 80);
        return QPromise<QString>::resolve(QString());
    }
}

QPromise<void> ConfigurationManager::updateFirmware(const TcosConfiguration &config, const QString &path) const
{

    qDebug() << Q_FUNC_INFO << path;
    if (path.isEmpty()) {
        return QPromise<void>::resolve();
    }

    QProcess *updateProcess = new QProcess();
    return QPromise<void>(
        [this, updateProcess, config, path](const QPromiseResolve<void> &resolve, const QPromiseReject<void> &reject) {
        QString md5sum = config.firmware().md5sum();

        connect(updateProcess, &QProcess::errorOccurred, [reject](QProcess::ProcessError error) {
            return reject(ProcessError({error, QLatin1String("Error starting update process")}));
        });

        connect(updateProcess, &QProcess::readyReadStandardOutput, [this, updateProcess]() {
            QStringList messages = QString::fromUtf8(updateProcess->readAllStandardOutput())
                    .split(QLatin1Char('\n'), QString::SkipEmptyParts);
            for (QString message : messages) {
                m_psplash->writeMessage(message, 90);
            }
        });

        connect(updateProcess, static_cast<void (QProcess::*)(int, QProcess::ExitStatus)>(&QProcess::finished),
                [updateProcess, reject, resolve](int exitCode) {
            if (exitCode != 0) {
                return reject(ProcessError({updateProcess->error(), QString(QLatin1String("Update failed with %1")).arg(exitCode)}));
            } else {
                return resolve();
            }
        });
        updateProcess->start(QLatin1String("/usr/bin/tcos-updater"), QStringList({path, md5sum}));
    })
    .delay(5000)
    .then([this](){
        return m_controller->reboot();
    })
    .finally([updateProcess](){
        updateProcess->deleteLater();
    });
}

bool ConfigurationManager::checkFirmwareUpdate(const QString &md5sum) const
{
    QFile localMd5sum(QLatin1String("/run/initramfs/image/md5sum.txt"));
    if (localMd5sum.open(QIODevice::ReadOnly)) {
        return localMd5sum.readAll().trimmed() == md5sum.toLatin1();
    } else {
        return false;
    }
}
