#include "tcosconnectionbase.h"
#include "tcosbrowserconnection.h"
#include "tcosinvalidconnection.h"
#include "tcosrdpconnection.h"

TcosConnectionBase::TcosConnectionBase(const QVariantMap &data) : m_configuration(data)
{
}

bool TcosConnectionBase::isValid() const
{
    if (m_configuration.isEmpty()) {
        return false;
    }

    if (type() == INVALID) {
        return false;
    }

    return true;
}

QSharedPointer<TcosConnectionBase> TcosConnectionBase::factory(const QVariantMap &data)
{
    if (data[QLatin1String("type")] == QLatin1String("rdp")) {
        return QSharedPointer<TcosConnectionBase>(new TcosRdpConnection(data));
    } else if (data[QLatin1String("type")] == QLatin1String("browser")) {
        return QSharedPointer<TcosConnectionBase>(new TcosBrowserConnection(data));
    } else {
        return QSharedPointer<TcosConnectionBase>(new TcosInvalidConnection(data));
    }
}

QVariantMap TcosConnectionBase::configuration() const
{
    return m_configuration;
}
