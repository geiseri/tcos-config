TEMPLATE = subdirs
SUBDIRS = \
    core \
    src \
    tests

CONFIG += ordered

DISTFILES += \
    README.md \
    browser_sample.ini \
    rdp_sample.ini \
    dhcp-config.png \
    dns-config.png


