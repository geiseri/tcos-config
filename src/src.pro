QT -= gui
QT += dbus network

CONFIG += c++11 console
CONFIG -= app_bundle
TARGET = tcos-config

CONFIG += link_pkgconfig
PKGCONFIG += qtsystemd

unit_files.files = tcos-config.service
unit_files.path = $$system(pkg-config --variable=systemdsystemunitdir systemd)
INSTALLS += unit_files

OTHER_FILES += $$unit_files.files
DISTFILES += $$unit_files.files

LIBS += -L$$OUT_PWD/../core/ -lcore
INCLUDEPATH += $$PWD/../core $$PWD/../core/qtpromise/include
DEPENDPATH += $$PWD/../core
PRE_TARGETDEPS += $$OUT_PWD/../core/libcore.a

SOURCES += main.cpp

target.path = /usr/bin
INSTALLS += target
