#include <QCoreApplication>
#include <QDebug>
#include <QScopedPointer>

#include <configurationmanager.h>

int main(int argc, char *argv[])
{
    QScopedPointer<QCoreApplication> app(new QCoreApplication(argc, argv));
    QScopedPointer<ConfigurationManager> mgr(new ConfigurationManager());
    QObject::connect(mgr.data(), &ConfigurationManager::finished, app.data(), &QCoreApplication::quit);

    return app->exec();
}
